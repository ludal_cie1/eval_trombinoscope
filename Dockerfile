# Utiliser une image Nginx légère
FROM nginx:alpine

# Copier les fichiers statiques et le dossier Back dans le répertoire de travail de Nginx
COPY Front /usr/share/nginx/html
COPY Back /usr/share/nginx/html/Back

# Exposer le port 80 (port par défaut pour HTTP)
EXPOSE 80