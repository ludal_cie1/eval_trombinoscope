document.addEventListener('DOMContentLoaded', () => {
    const trombinoscopeDiv = document.getElementById('trombinoscope');
    const searchInput = document.getElementById('searchInput');
    const resetFilterBtn = document.getElementById('resetFilterBtn');
    const ajouterBtn = document.getElementById('ajouterBtn');

    console.log(trombinoscopeDiv); // Débogage

    if (trombinoscopeDiv) {
        // Ce code est spécifique à la page principale (index.html)
        fetch('../Back/bdd.json')
            .then(response => response.json())
            .then(data => {
                generateCards(data.trombinoscope);
                searchInput.addEventListener('input', () => {
                    const searchTerm = searchInput.value.trim().toLowerCase();
                    console.log(searchTerm); // Débogage
                    if (searchTerm.length >= 3) {
                        filterCards(data.trombinoscope, searchTerm);
                    } else {
                        trombinoscopeDiv.innerHTML = '';
                    }
                });
                resetFilterBtn.addEventListener('click', resetFilter);
                ajouterBtn.addEventListener('click', () => {
                    window.location.href = 'add.html';
                });
            });
    }

    const isAddPage = window.location.href.includes('add.html');
    if (isAddPage) {
        // Ce code est spécifique à la page d'ajout (add.html)
        const ajouterForm = document.getElementById('ajouterForm');
        console.log(ajouterForm); // Débogage
        ajouterForm.style.display = 'block';
        document.getElementById('validerAjout').addEventListener('click', () => {
            const nom = document.getElementById('nom').value;
            const prenom = document.getElementById('prenom').value;
            const adresse = document.getElementById('adresse').value;
            const email = document.getElementById('email').value;
            const telephone = document.getElementById('telephone').value;
            const fonction = document.getElementById('fonction').value;
            const date = document.getElementById('naissance').value;
            const photo = document.getElementById('photo').value;

            console.log(nom, prenom, adresse, email, telephone, fonction, date, photo); // Débogage

            const photoPath = `assets/profiles/${photo}`;

            console.log(photoPath); // Débogage

            data.trombinoscope.push({
                "Nom": nom,
                "Prénom": prenom,
                "adresse": adresse,
                "email": email,
                "tel": telephone,
                "fonction": fonction,
                "naissance": date,
                "photo": photoPath
            });

            document.getElementById('ajouterForm').style.display = 'none';
            document.getElementById('nom').value = '';
            document.getElementById('prenom').value = '';
            document.getElementById('adresse').value = '';
            document.getElementById('email').value = '';
            document.getElementById('telephone').value = '';
            document.getElementById('fonction').value = '';
            document.getElementById('naissance').value = '';
            document.getElementById('photo').value = '';

            resetFilter();
        });
    }
});


function generateCards(persons) {
    const trombinoscopeDiv = document.getElementById('trombinoscope');
    if (!trombinoscopeDiv) {
        console.error('L\'élément avec l\'ID "trombinoscope" n\'a pas été trouvé.');
        return;
    }

    persons.forEach((person, index) => {
        const cardDiv = document.createElement('div');
        cardDiv.classList.add('card');

        const img = document.createElement('img');
        img.src = person.photo;
        img.alt = `${person.Prénom} ${person.Nom}`;
        img.classList.add('card-photo');
        cardDiv.appendChild(img);

        const infoDiv = document.createElement('div');
        infoDiv.classList.add('card-info');

        const name = document.createElement('p');
        name.textContent = `${person.Prénom} ${person.Nom}`;
        infoDiv.appendChild(name);

        const position = document.createElement('p');
        position.textContent = person.fonction;
        infoDiv.appendChild(position);

        // const deleteBtn = document.createElement('button');
        // deleteBtn.textContent = 'Supprimer';
        // deleteBtn.addEventListener('click', () => deletePerson(index));
        // infoDiv.appendChild(deleteBtn);

        cardDiv.appendChild(infoDiv);

        cardDiv.addEventListener('click', () => showPersonalInfo(person));

        trombinoscopeDiv.appendChild(cardDiv);
    });
}

function resetFilter() {
    location.reload();
}

function filterCards(persons, searchTerm) {
    const filteredPersons = persons.filter(person => {
        return (
            person.adresse.toLowerCase().includes(searchTerm) ||
            person.fonction.toLowerCase().includes(searchTerm)
        );
    });

    const trombinoscopeDiv = document.getElementById('trombinoscope');
    trombinoscopeDiv.innerHTML = '';
    generateCards(filteredPersons);
}

function showPersonalInfo(person) {
    const overlay = document.getElementById('overlay');
    const personalInfoDiv = document.getElementById('personalInfo');

    personalInfoDiv.innerHTML = `
        <span class="close-btn" onclick="closeOverlay()">X</span>
        <div class="personal-details">
            <img src="${person.photo}" alt="${person.Prénom} ${person.Nom}" class="personal-photo">
            <div class="personal-text">
                <h2>${person.Prénom} ${person.Nom}</h2>
                <p><strong>Email:</strong> ${person.email}</p>
                <p><strong>Téléphone:</strong> ${person.tel}</p>
                <p><strong>Adresse:</strong> ${person.adresse}</p>
                <p><strong>Date de naissance:</strong> ${person.naissance}</p>
                <p><strong>Ville:</strong> ${getCityFromAddress(person.adresse)}</p>
                <p><strong>Fonction:</strong> ${person.fonction}</p>
            </div>
        </div>
    `;

    overlay.style.display = 'flex';
}

function closeOverlay() {
    const overlay = document.getElementById('overlay');
    overlay.style.display = 'none';
}

function getCityFromAddress(address) {
    const cities = ['Paris', 'Neuilly-sur-Seine', 'Levallois-Perret', 'Clichy', 'Asnières-sur-Seine', 'Issy-les-Moulineaux'];
    const foundCity = cities.find(city => address.toLowerCase().includes(city.toLowerCase()));
    return foundCity || 'Non spécifiée';
}

function deletePerson(index) {
    const confirmation = confirm("Êtes-vous sûr de vouloir supprimer cette personne ?");
    if (confirmation) {
        data.trombinoscope.splice(index, 1);
        resetFilter();
    }
}
